<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Maillot $model */

$this->title = $model->código;
$this->params['breadcrumbs'][] = ['label' => 'Maillots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="maillot-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'código' => $model->código], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'código' => $model->código], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'código',
            'tipo',
            'color',
            'premio',
        ],
    ]) ?>

</div>
